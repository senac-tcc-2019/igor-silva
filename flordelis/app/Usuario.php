<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
  // define campos que podem ser incluídos / alterados na tabela pelos
  // métodos do Laravel
  protected $fillable = array('nome', 'sobrenome', 'telefone', 'genero', 'data_nascimento',
                              'endereco', 'numero', 'complemento', 'cep', 'email', 'password');

  // indica que esta model (tabela produtos) não utiliza os campos
  // created_at e updated_at
  public $timestamps = false;

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = ['password', 'remember_token',];
}
