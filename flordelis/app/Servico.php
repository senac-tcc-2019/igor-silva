<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servico extends Model
{
    // define campos que podem ser incluídos / alterados na tabela pelos
    // métodos do Laravel
    protected $fillable = array('nome', 'valor', 'tempo_duracao', 'descricao');

    // indica que esta model (tabela produtos) não utiliza os campos
    // created_at e updated_at
    public $timestamps = false;

}
