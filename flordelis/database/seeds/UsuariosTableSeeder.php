<?php

use Illuminate\Database\Seeder;

class UsuariosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('usuarios')->insert([
        'nome' => 'Marianne',
        'sobrenome' => 'Silva',
        'Telefone' => '981252658',
        'genero' => 'feminino',
        'data_nascimento' => '19730603',
        'endereco' => 'Avenida Domingos de Almeida',
        'numero' => '2773',
        'complemento' => 'casa',
        'cep' => '96085470',
        'email' => 'marianne@hotmail.com',
        'password' => '123456',
    ]);
    }
}
