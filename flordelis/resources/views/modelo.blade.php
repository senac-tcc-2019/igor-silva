<!DOCTYPE html>
<html lang="en">
<head>
  <title>Cadastro de Serviços</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h3>Painel Administrativo</h3>

</div>

<nav class="navbar navbar-expand-sm bg-primary navbar-dark">
  <ul class="navbar-nav">
    <li class="nav-item active">
      <h3>Serviços</h3>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{route ('home')}}">Home</a>
      <!--   <a href="{{ route('login') }}" class="btn btn-primary pull-right"
                role="button">Home</a>  -->
    </li>
    <li class="nav-item">
     <a class="nav-link" href="/servicos">Serviços</a>
    </li>

    </li>
    <li class="nav-item">
      <a class="nav-link" href="/usuarios">Clientes</a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="/funcionarios">Funcionarios</a>
    </li>

  </ul>
</nav>

  @yield('conteudo')

</body>
</html>
