<?php

use Illuminate\Database\Seeder;

class FuncionariosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('funcionarios')->insert([
        'nome' => 'Francine',
        'sobrenome' => 'Silva',
        'Telefone' => '981252659',
        'genero' => 'feminino',
        'data_nascimento' => '19970315',
        'endereco' => 'Avenida Domingos de Almeida',
        'numero' => '1762',
        'complemento' => 'casa',
        'cep' => '96085470',
        'email' => 'francine@hotmail.com',
        'salario' => '500',
        'data_admissao' => '20180110',
        'funcao' => 'manicure',
        'password' => '123456',
    }
}
