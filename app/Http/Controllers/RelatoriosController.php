<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Usuario;


class RelatoriosController extends Controller
{



    public function index()
    {
      Carbon::setLocale('pt_BR');
      // retorna os usuários cadastrados no ultimo mes
      $usuarios = Usuario::whereMonth('created_at', Carbon::now()->month)->get();
      $currentMonth = now()->format('M');
      return view('relatorios.index', compact(['usuarios', 'currentMonth']));

    }
}
