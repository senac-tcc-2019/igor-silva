<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Funcionario;

class FuncionarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // recupera todos os registros da tabela funcionarios
        $dados = Funcionario::all();

        return view('funcionarios_lista', ['funcionarios' => $dados]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('funcionarios_form', ['acao' => 1]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // unique: indica que o campo deve ser único na
        //         tabela funcionarios
        // min e max: número min e max de caracteres
        // numeric: que o campo deve ser numérico

        $validatedData = $request->validate([
            'nome' => 'required|unique:funcionarios|min:3|max:100',
            'sobrenome' => 'required|min:3|max:10',
            'telefone' => 'required|min:8|max:18',
            'data_admissao' => 'required|date',
            'salario' => 'required'

        ]);

        $dados = $request->all();

        $prod = Funcionario::create($dados);

        if ($prod) {
            return redirect()->route('funcionarios.index')
            ->with('status', 'Funcionario ' . $request->nome . ' inserido com sucesso!!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $reg = Funcionario::find($id);

        return view('funcionarios_form', ['reg' => $reg, 'acao' => 2]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reg = Funcionario::find($id);

        return view('funcionarios_form', ['reg' => $reg, 'acao' => 3]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'nome' => 'required|unique:funcionarios,nome,'.$id.'|min:3|max:100',
            'sobrenome' => 'required|min:3|max:10',
            'telefone' => 'required|min:8|max:18',
            'data_admissao' => 'required|date',
            'salario' => 'required'
        ]);

        // posiciona no registro a ser alterado
        $reg = Funcionario::find($id);

        // obtém os campos de formulário
        $dados = $request->all();

        // altera o registro passando os novos dados
        $alt = $reg->update($dados);

        if ($alt) {
          return redirect()->route('funcionarios.index')
            ->with('status', 'Funcionario ' . $request->nome . ' alterado com sucesso!!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reg = Funcionario::find($id);
        if ($reg->delete()) {
            return redirect()->route('funcionarios.index')
            ->with('status', 'Funcionario ' . $reg->nome . ' excluído corretamente!!');
        }
    }

    public function pesq(Request $request) {
        // recupera todos os registros da tabela funcionarios
        $dados = Funcionario::where('nome', 'like','%'.$request->palavra.'%')
                          ->orwhere('sobrenome','like','%'.$request->palavra.'%')
                          ->get();

        return view('funcionarios_lista', ['funcionarios' => $dados,
                         'palavra' => $request->palavra]);
    }
}
