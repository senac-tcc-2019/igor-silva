<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Servico;
use App\Usuario;
use App\Event;


class AgendamentosController extends Controller
{

  public function index ()
  {
    $usuarios = Usuario::get();
    $servicos = Servico::get();

    return view('agendamentos.index', compact(['usuarios', 'servicos']));
  }
  public function store(Request $request)
  {
    $validatedData = $request->validate([
        'title' => 'required',
        'usuario_id' => 'required',
        'start_date' => 'required',

    ]);

    $dados = $request->all();
    $dados['end_date'] =  $dados['start_date'];

    $prod = Event::create($dados);

    if ($prod) {
        return redirect()->route('agendamentos.index')
        ->with('status', 'Agendamento para o usuário  ' . $request->nome . ' inserido com sucesso!!');
    }
  }
}
