<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Funcionario extends Model
{
  protected $fillable = array('nome', 'sobrenome', 'telefone', 'genero', 'data_nascimento',
                              'endereco', 'numero', 'complemento', 'cep', 'email', 'password',
                              'salario', 'data_admissao', 'funcao');
}
