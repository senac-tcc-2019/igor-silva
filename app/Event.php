<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Usuario;
class Event extends Model
{
    protected $fillable = ['usuario_id', 'title','start_date','end_date'];


    public function usuario(){
       return $this->belongsTo(Usuario::class, 'usuario_id');
    }
}
