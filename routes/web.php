<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//Route::get('revisao', function () {
//return view('revisao');
//});

Route::resource('servicos', 'ServicoController');
Route::post('servicospesq', 'ServicoController@pesq')
        ->name('servicos.pesq');

Route::resource('clientes', 'UsuarioController');
Route::post('clientespesq', 'UsuarioController@pesq')
        ->name('clientes.pesq');

Route::resource('funcionarios', 'FuncionarioController');
Route::post('funcioanriopesq', 'FuncionarioController@pesq')
        ->name('funcionarios.pesq');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Route::get('/home2', 'Home2Controller@index')->name('home2');

Route::get('/events', 'EventController@index');

// Route::resource('carros', 'Admin\CarroController');
//     Route::get('carrosgraf', 'Admin\CarroController@graf')
//             ->name('carros.graf');

// Route::get('/servicos', 'ServicoController@index')->name('servicos');
//
// Route::get('/usuarios', 'UsuarioController@index')->name('usuarios');
//
// Route::get('/funcionarios', 'FuncionarioController@index')->name('funcionario');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/relatorios', 'RelatoriosController@index');
Route::get('/agendamentos', 'AgendamentosController@index')->name('agendamentos.index');
Route::post('/agendamentos', 'AgendamentosController@store')->name('agendamentos.save');
