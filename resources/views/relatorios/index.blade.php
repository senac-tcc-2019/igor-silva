@extends('adminlte::page')

@section('title', 'Relatórios')

@section('content_header')
    <h1>Lista de usuários cadastrados no mês de {{ $currentMonth }}</h1>
@stop

@section('content')
  <div class="row">
    <div class="col-md-12">

      <table class="table table-hover">
        <thead>
          <tr>
            <th>Nome</th>
            <th>Sobrenome</th>
            <th>Telefone</th>
            <th>E-mail</th>
            <th>Endereço</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($usuarios as $u)
            <tr>
              <td>{{$u->nome}}</td>
              <td>{{$u->sobrenome}}</td>
              <td>{{$u->telefone}}</td>
              <td>{{$u->email}}</td>
              <td>{{$u->endereco}}</td>
              <td>{{$u->created_at->diffForHumans()}}</td>
            </tr>

          @endforeach
        </tbody>
      </table>
    </div>
  </div>
@stop
