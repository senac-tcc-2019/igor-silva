@extends('adminlte::page')

@section('title', 'Serviços')

@section('content_header')

<div class="container">

  <div class="row" style="margin-top: 10px">
  <div class="col-sm-6">
    <h2>Cadastro de servicos</h2>
  </div>

  <div class="col-sm-4">
     <form method="POST"
           class="form-inline"
           action="{{ route('servicos.pesq') }}">
       {{ csrf_field() }}
       <input type="text" class="form-control"
              name="palavra"
              placeholder="Palavra do filtro"> &nbsp;
       <input type="submit" class="btn btn-success"
              value="Ok">
     </form>
  </div>

  <div class="col-sm-2">
      <a href="{{ route('servicos.index') }}"
         class="btn btn-warning" role="button">
          Todos</a>

      <a href="{{ route('servicos.create') }}"
         class="btn btn-info" role="button">
           Novo</a>
  </div>

  </div>

  @if (session('status'))
  <div class="alert alert-success">
      {{ session('status') }}
  </div>
  @endif

  <table class="table table-hover">
    <thead>
      <tr>
        <th>Serviço</th>
        <th>Valor</th>
        <th>Duração</th>
        <th>Descrição</th>
        <th>Ações</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($servicos as $p)
        <tr>
          <td>{{$p->nome}}</td>
          <td>{{$p->valor}}</td>
          <td>{{$p->tempo_duracao}}</td>
          <td>{{$p->descricao}}</td>
          <td>
           <a href="{{ route('servicos.show', $p->id) }}"
              class="btn btn-success btn-sm" role="button">Consultar</a>

           <a href="{{ route('servicos.edit', $p->id) }}"
              class="btn btn-warning btn-sm" role="button">Alterar</a>

           <form method="POST" action="{{ route('servicos.destroy', $p->id) }}"
                 style="display: inline-block;"
                 onsubmit="return confirm('Confirma Exclusão?') ">
            {{ method_field('DELETE') }}
            {{ csrf_field() }}
              <button type="submit" class="btn btn-danger btn-sm">
                Excluir</button>
           </form>

          </td>
        </tr>
      @empty
        <tr><td colspan=5>
          Não há servicos cadastrados
          ou com a palavra <b>{{ $palavra or '' }}</b>
          informada na pesquisa </td></tr>
      @endforelse
    </tbody>
  </table>

</div>

@stop
