@extends('adminlte::page')

@section('title', 'Agendamentos')

@section('content_header')
    <h1>Criar Agendamento</h1>
@stop

@section('content')
  <div class="row">
    <div class="col-md-12">
        <form class="" action="{{ route('agendamentos.save')}}" method="post">
          {{ csrf_field() }}
          <div class="form-group">
            <label for="usuario_id">Cliente</label>
            <select class="form-control" name="usuario_id">
              <option value="" style="display:none" selected></option>
              @foreach($usuarios as $usuario)
                <option value="{{ $usuario->id }}">{{ $usuario->nome }} {{  $usuario->sobrenome  }}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <label for="usuario_id">Serviço</label>
            <select class="form-control" name="title">
              <option value="" style="display:none" selected></option>
              @foreach($servicos as $servico)
                <option value="{{ $servico->nome }}">{{ $servico->nome }}</option>
              @endforeach
            </select>
        </div>
          <div class="form-group">
            <label for="start_date">Data</label>
            <input class="form-control" type="date" name="start_date" value="">
          </div>
          <button type="submit" class="btn btn-success" name="button">Cadastrar</button>
        </form>

    </div>
  </div>
@stop

@push('js')
 <script type="text/javascript" src="/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
 <link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.min.css" />
 <link rel="stylesheet" href="/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />
@endpush
