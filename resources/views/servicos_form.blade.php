@extends('adminlte::page')

@section('title', 'Consulta de Serviços')

@section('css')
<style media="screen">
  .container{
    max-width: 100%;
  }
</style>
@stop

@section('content_header')

<div class="container">

  <div class="row" style="margin-top: 10px">

  @if ($errors->any())
    <div class="col-sm-12 alert alert-danger">
      <ul>
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
      </ul>
    </div>
  @endif

  <div class="col-sm-11">

  @if ($acao == 1)
    <h2>Inclusão de servicos</h2>
  @elseif ($acao == 2)
    <h2>Consulta de servicos</h2>
  @else
    <h2>Alteração de servicos</h2>
  @endif

  </div>
  <div class="col-sm-1">
  <a href="{{ route('servicos.index') }}" class="btn btn-info" role="button">
    Voltar</a>
  </div>
  </div>

  @if ($acao == 1)
    <form action="{{ route('servicos.store') }}" method="POST">
  @elseif ($acao == 3)
    <form action="{{ route('servicos.update', $reg->id) }}" method="POST">
    {{ method_field('PUT') }}
  @endif

    {{ csrf_field() }}

    <div class="form-group">
      <label for="nome">Nome:</label>
      <input type="text" class="form-control" id="nome" name="nome"
             value="{{ $reg->nome or old('nome') }}">
    </div>

    <div class="form-group">
      <label for="valor">Valor:</label>
      <input type="text" class="form-control money" id="valor" name="valor"
             value="{{ $reg->valor or old('valor') }}">
    </div>

    <div class="form-group">
      <label for="tempo_duracao">Duração:</label>
      <input type="text" class="form-control time" id="tempo_duracao" name="tempo_duracao"
             value="{{ $reg->tempo_duracao or old('tempo_duracao') }}">
    </div>

    <div class="form-group">
      <label for="descricao">Descrição:</label>
      <input type="text" class="form-control" id="descricao" name="descricao"
             value="{{ $reg->descricao or old('descricao') }}">
    </div>

    @if($acao == 1 or $acao == 3)
      <button type="submit" class="btn btn-primary">Enviar</button>
      <button type="reset" class="btn btn-success">Limpar</button>
    @endif

  </form>
@stop

@section('js')
<script       src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
<script src="{{ asset('js/jquery.mask.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){
var SPMaskBehavior = function (val) {
  return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
},
spOptions = {
  onKeyPress: function(val, e, field, options) {
      field.mask(SPMaskBehavior.apply({}, arguments), options);
    }
};

$('.telefone-mask').mask(SPMaskBehavior, spOptions);
});
$('.cep').mask('00000-000');
 $('.money').mask('000.000.000.000.000,00', {reverse: true});
 $('.time').mask('00:00:00');
</script>
@stop
