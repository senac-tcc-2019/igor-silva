@extends('adminlte::page')



<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>


@section('content')
   <div class="row">
       <div class="col-md-12">
           <div class="panel panel-default">
               <div class="panel-heading">Agenda</div>

                <div class="panel-body">
                   {!! $calendar->calendar() !!}
               </div>
           </div>
       </div>
   </div>
@endsection

@section('adminlte_js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
<script src="{{asset('js/locale-all.js')}}"></script>
<script src="{{asset('js/pt-br.js')}}"></script>

{!! $calendar->script() !!}
@endsection
